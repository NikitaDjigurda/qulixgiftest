package com.qulix.sergey.qulixgiftest.activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.qulix.sergey.qulixgiftest.R;
import com.qulix.sergey.qulixgiftest.adapter.GifAdapter;
import com.qulix.sergey.qulixgiftest.adapter.RecyclerLazyLoadAdapter;
import com.qulix.sergey.qulixgiftest.loader.LoaderResponse;
import com.qulix.sergey.qulixgiftest.loader.SearchLoader;
import com.qulix.sergey.qulixgiftest.loader.TrendingLoader;
import com.qulix.sergey.qulixgiftest.response.GiphyResponse;

public class MainActivity extends AppCompatActivity {

    private static final int PAGE_LIMIT = 10;

    private static final String BUNDLE_OFFSET = "BUNDLE_OFFSET";
    private static final String BUNDLE_SEARCH_TERM = "BUNDLE_SEARCH_TERM";

    private GifAdapter adapter;
    private String searchTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @SuppressWarnings("ConstantConditions")
    private void init() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GifAdapter(this, null, R.layout.lazy_load_footer);
        recyclerView.setAdapter(adapter);
        adapter.setLazyListener(new RecyclerLazyLoadAdapter.LazyAdapterListener() {
            @Override
            public void onNextLoad(int page, boolean reloadMode) {
                if (searchTerm == null || searchTerm.isEmpty()) {
                    loadTrends(page);
                } else {
                    loadSearch(page);
                }
            }

            @Override
            public void onNoMoreResults() {}
        });
        adapter.performLoad();
    }

    private void loadTrends(int page) {
        int offset = page * PAGE_LIMIT;
        Bundle args = new Bundle();
        args.putInt(BUNDLE_OFFSET, offset);
        getLoaderManager().restartLoader(R.id.loader_trending + page, args,
                new LoaderManager.LoaderCallbacks<LoaderResponse>() {
                    @Override
                    public Loader<LoaderResponse> onCreateLoader(int id, Bundle args) {
                        int offset = args.getInt(BUNDLE_OFFSET, 0);
                        return new TrendingLoader(MainActivity.this, args, PAGE_LIMIT, offset);
                    }

                    @Override
                    public void onLoadFinished(Loader<LoaderResponse> loader, LoaderResponse data) {
                        GiphyResponse trendingResponse = data.getTypedResult();
                        adapter.onLoadSuccess(trendingResponse.getGifList());
                    }

                    @Override
                    public void onLoaderReset(Loader<LoaderResponse> loader) {}
                });
    }

    private void loadSearch(int page) {
        int offset = page * PAGE_LIMIT;
        Bundle args = new Bundle();
        args.putInt(BUNDLE_OFFSET, offset);
        args.putString(BUNDLE_SEARCH_TERM, searchTerm);
        getLoaderManager().restartLoader(R.id.loader_search + page, args,
                new LoaderManager.LoaderCallbacks<LoaderResponse>() {
                    @Override
                    public Loader<LoaderResponse> onCreateLoader(int id, Bundle args) {
                        int offset = args.getInt(BUNDLE_OFFSET, 0);
                        String searchTerm = args.getString(BUNDLE_SEARCH_TERM);
                        return new SearchLoader(MainActivity.this, args, searchTerm, PAGE_LIMIT, offset);
                    }

                    @Override
                    public void onLoadFinished(Loader<LoaderResponse> loader, LoaderResponse data) {
                        GiphyResponse trendingResponse = data.getTypedResult();
                        adapter.onLoadSuccess(trendingResponse.getGifList());
                    }

                    @Override
                    public void onLoaderReset(Loader<LoaderResponse> loader) {

                    }
                });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            boolean queryChanged;

            @Override
            public boolean onQueryTextSubmit(String query) {
                if (queryChanged) {
                    // TODO: indeterminate
                    searchTerm = query;
                    adapter.clear();
                    adapter.performLoad();
                    searchView.onActionViewCollapsed();
                    setTitle(query);
                }
                queryChanged = false;
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                queryChanged = true;
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (searchView.getQuery().toString().isEmpty()) {
                    searchTerm = null;
                    setTitle(R.string.app_name);
                    adapter.clear();
                    adapter.performLoad();
                }
                return false;
            }
        });
        return true;
    }
}