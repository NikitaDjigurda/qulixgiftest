package com.qulix.sergey.qulixgiftest.loader;

import android.content.Context;
import android.os.Bundle;

import com.qulix.sergey.qulixgiftest.GifApp;
import com.qulix.sergey.qulixgiftest.response.GiphyResponse;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

public class SearchLoader extends BaseLoader<GiphyResponse> {

    private static final String[] ratingsArray = {
            "y", "g", "pg"
    };
    private static final List<String> ratingsList = Arrays.asList(ratingsArray);

    private final String term;
    private final int limit;
    private final int offset;

    public SearchLoader(Context context, Bundle args, String term, int limit, int offset) {
        super(context, args);
        this.term = term;
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    protected Call<GiphyResponse> getCall() {
        return GifApp.getRetrofit().create(GiphyResponse.Service.class).search(term, limit, offset, ratingsList);
    }
}
