package com.qulix.sergey.qulixgiftest.loader;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class LoaderResponse {

    @Nullable
    private Object result;
    @NonNull
    private Bundle args;
    private boolean success;
    private Exception exception;

    public LoaderResponse(@Nullable Object result, @NonNull Bundle args) {
        this(result, args, false);
    }

    public LoaderResponse(@Nullable Object result, @NonNull Bundle args, boolean forceSuccess) {
        setResult(result);
        this.args = args;
        if (forceSuccess) success = true;
    }

    public <T> T getTypedResult() {
        if (result == null) {
            return null;
        }
        //noinspection unchecked
        return (T) result;
    }

    public void setResult(@Nullable Object result) {
        this.result = result;
        success = result != null;
    }

    public boolean isSuccess() {
        return success;
    }

    public Exception getException() {
        return exception;
    }

    @NonNull
    public Bundle getArgs() {
        return args;
    }
}
