package com.qulix.sergey.qulixgiftest.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class RecyclerLazyLoadAdapter<T> extends BaseRecyclerAdapter<T, BaseRecyclerAdapter.BaseViewHolder<T>> {

    private static final int FOOTER_VIEW = 198;

    private static final int LOAD_NEXT = 747;
    private static final int NO_MORE_RESULTS = 97;
    private static final int RELOAD_ALL = 942;

    private LazyAdapterListener lazyListener;
    private int currentPage = 0;
    private int lastQueriedPosition = 0;
    private boolean hasMoreResults = true;
    private LoadHandler loadHandler;
    private int footerLayout;

    public RecyclerLazyLoadAdapter(Context context, List<T> items, @LayoutRes int footerLayout) {
        super(context, items);
        loadHandler = new LoadHandler();
        this.footerLayout = footerLayout;
    }

    public interface LazyAdapterListener {
        void onNextLoad(int page, boolean reloadMode);
        void onNoMoreResults();
    }

    protected abstract BaseViewHolder<T> createRegularViewHolder(ViewGroup parent, int viewType);

    @Override
    public int getItemCount() {
        if (super.getItemCount() == 0) {
            return 0;
        } else {
            return hasMoreResults ? super.getItemCount() + 1 : super.getItemCount();
        }
    }

    @Override
    public T getItem(int position) {
        return needShowFooter(position) ? super.getItem(position - 1) : super.getItem(position);
    }

    @Override
    public int getItemViewType(int position) {
        return needShowFooter(position) ? FOOTER_VIEW : super.getItemViewType(position);
    }

    @Override
    public final BaseViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == FOOTER_VIEW ? createFooterViewHolder(parent) : createRegularViewHolder(parent, viewType);
    }

    private BaseViewHolder<T> createFooterViewHolder(ViewGroup parent) {
        View footer = LayoutInflater.from(getContext()).inflate(footerLayout, parent, false);
        return new BaseViewHolder<T>(footer) {
            @Override
            public void hold(T item) {
                // it's a footer holder - lets try to load more
                if (hasMoreResults) {
                    if (lastQueriedPosition != getItemCount()) {
                        lastQueriedPosition = getItemCount();
                        currentPage++;
                        sendMessage(LOAD_NEXT, currentPage);
                    }
                } else {
                    sendMessage(NO_MORE_RESULTS, -1);
                }
            }
        };
    }

    public void performLoad() {
        sendMessage(LOAD_NEXT, currentPage);
    }

    /**
     * Performs to reload all pages, that previously was loaded
     */
    public void performInvalidate() {
        for (int i = 0; i <= currentPage; i++) {
            sendMessage(RELOAD_ALL, i);
        }
    }

    private void sendMessage(int what, int page) {
        Message message = new Message();
        message.obj = lazyListener;
        message.what = what;
        if (page != -1) {
            message.arg1 = page;
        }
        loadHandler.sendMessage(message);
    }

    public void setLazyListener(LazyAdapterListener lazyListener) {
        this.lazyListener = lazyListener;
    }

    @Override
    public void clear() {
        super.clear();
        currentPage = 0;
        lastQueriedPosition = 0;
        hasMoreResults = true;
    }

    @Override
    public void addAll(@NonNull Collection<? extends T> collection) {
        if (!collection.isEmpty()) {
            int start = getItems().size();
            getItems().addAll(collection);
            notifyItemRangeInserted(start, collection.size());
            notifyItemChanged(getItemCount() - 1);
        } else {
            if (lazyListener != null) {
                lazyListener.onNoMoreResults();
            }
            int footerPosition = getItemCount() - 1;
            if (footerPosition != -1) {
                notifyItemRemoved(footerPosition);
            }
        }
        hasMoreResults = !collection.isEmpty();
    }

    public void setHasNoMoreResults() {
        hasMoreResults = false;
        notifyItemChanged(getItemCount() - 1);
    }

    public boolean hasMoreResults() {
        return hasMoreResults;
    }

    protected boolean needShowFooter(int position) {
        return hasMoreResults && position == getItemCount() - 1;
    }

    public void onLoadSuccess(List<T> value) {
        if (value == null) {
            // noinspection unchecked
            addAll(Collections.EMPTY_LIST);
        } else {
            addAll(value);
        }
    }

    private static class LoadHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                LazyAdapterListener listener = (LazyAdapterListener) msg.obj;
                switch (msg.what) {
                    case LOAD_NEXT:
                        listener.onNextLoad(msg.arg1, false);
                        break;
                    case NO_MORE_RESULTS:
                        listener.onNoMoreResults();
                        break;
                    case RELOAD_ALL:
                        listener.onNextLoad(msg.arg1, true);
                }
            } catch (Exception e) {
                Log.d("logd", "Adapter not ready, retrying...");
                e.printStackTrace();
                sendMessageDelayed(Message.obtain(msg), 50);
            }
        }
    }
}
