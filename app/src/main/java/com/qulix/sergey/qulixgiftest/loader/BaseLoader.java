package com.qulix.sergey.qulixgiftest.loader;

import android.content.Context;
import android.content.Loader;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseLoader<T> extends Loader<LoaderResponse> {

    private Bundle args;
    private LoaderResponse loaderResponse;

    public BaseLoader(Context context, Bundle args) {
        super(context);
        this.args = args != null ? args : Bundle.EMPTY;
    }

    protected abstract Call<T> getCall();

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        getCall().enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful()) {
                    deliverResult(new LoaderResponse(response.body(), args));
                } else {
                    deliverResult(null);
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                deliverResult(null);
            }
        });
    }

    @Override
    public void deliverResult(LoaderResponse data) {
        loaderResponse = data;
        super.deliverResult(data);
    }

    @Override
    public void registerListener(int id, OnLoadCompleteListener<LoaderResponse> listener) {
        super.registerListener(id, listener);
        if (!isReset() && loaderResponse != null) {
            deliverResult(loaderResponse);
        }
    }
}
