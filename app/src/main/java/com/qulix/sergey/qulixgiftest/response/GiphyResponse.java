package com.qulix.sergey.qulixgiftest.response;

import com.google.gson.annotations.SerializedName;
import com.qulix.sergey.qulixgiftest.data.GifData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class GiphyResponse {

    @SerializedName("data")
    private List<GifData> gifList;

    private Pagination pagination;

    public List<GifData> getGifList() {
        return gifList;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public interface Service {

        @GET("/v1/gifs/trending")
        Call<GiphyResponse> trending(@Query("limit") int limit,
                                     @Query("offset") int offset);

        @GET("/v1/gifs/search")
        Call<GiphyResponse> search(@Query("q") String term,
                                   @Query("limit") int limit,
                                   @Query("offset") int offset,
                                   @Query("rating") List<String> rating);
    }

    private class Pagination {

        private int count;
        private int offset;

        public int getCount() {
            return count;
        }

        public int getOffset() {
            return offset;
        }
    }
}
