package com.qulix.sergey.qulixgiftest.loader;

import android.content.Context;
import android.os.Bundle;

import com.qulix.sergey.qulixgiftest.GifApp;
import com.qulix.sergey.qulixgiftest.response.GiphyResponse;

import retrofit2.Call;

public class TrendingLoader extends BaseLoader<GiphyResponse> {

    private final int limit;
    private final int offset;

    public TrendingLoader(Context context, Bundle args, int limit, int offset) {
        super(context, args);
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    protected Call<GiphyResponse> getCall() {
        return GifApp.getRetrofit().create(GiphyResponse.Service.class).trending(limit, offset);
    }
}
