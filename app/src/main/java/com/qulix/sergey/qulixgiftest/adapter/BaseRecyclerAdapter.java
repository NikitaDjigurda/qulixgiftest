package com.qulix.sergey.qulixgiftest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseRecyclerAdapter<T, VH extends BaseRecyclerAdapter.BaseViewHolder<T>> extends RecyclerView.Adapter<VH> {

    private List<T> items;
    private Context context;
    protected OnItemClickListener<T> clickListener;

    public interface OnItemClickListener<T> {
        void onItemClicked(T item);
    }

    public BaseRecyclerAdapter(Context context, @Nullable List<T> items) {
        if (items != null) {
            this.items = items;
        } else {
            this.items = new ArrayList<>();
        }
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    protected Context getContext() {
        return context;
    }

    public void addAll(@NonNull Collection<? extends T> collection) {
        int start = getItemCount();
        items.addAll(collection);
        notifyItemRangeInserted(start, collection.size());
    }

    public void add(T item) {
        int position = items.size();
        add(item, position);
    }

    public void add(T item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void replace(T item) {
        int index = items.lastIndexOf(item);
        items.set(index, item);
        notifyItemChanged(index);
    }

    public void replace(T oldItem, T newItem) {
        int index = items.lastIndexOf(oldItem);
        items.set(index, newItem);
        notifyItemChanged(index);
    }

    public void replaceAtPosition(T newItem, int position) {
        items.set(position, newItem);
        notifyItemChanged(position);
    }

    public void removeItem(T item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int itemCount = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    public void setItems(List<T> newItems) {
        clear();
        addAll(newItems);
    }

    public void setItem(T newItem) {
        ArrayList<T> newItems = new ArrayList<>();
        newItems.add(newItem);
        setItems(newItems);
    }

    protected List<T> getItems() {
        return items;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        final T item = getItem(position);
        holder.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onItemClicked(item);
                }
            }
        });
        holder.hold(item);
    }

    public void setOnItemClickListener(OnItemClickListener<T> listener) {
        this.clickListener = listener;
    }

    public static abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

        private View itemView;

        public BaseViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public abstract void hold(T item);

        public void setClickListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }
    }
}
