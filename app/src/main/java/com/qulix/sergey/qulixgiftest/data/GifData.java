package com.qulix.sergey.qulixgiftest.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GifData {

    private String type;
    private String id;
    private Images images;
    @SerializedName("trending_datetime")
    private Date trended;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return images.original.url;
    }

    public Date getTrended() {
        return trended;
    }

    private static class Images {

        private Original original;
    }

    private static class Original {

        private String url;
    }
}
