package com.qulix.sergey.qulixgiftest.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.qulix.sergey.qulixgiftest.R;
import com.qulix.sergey.qulixgiftest.data.GifData;

import java.util.Date;
import java.util.List;

public class GifAdapter extends RecyclerLazyLoadAdapter<GifData> {

    public GifAdapter(Context context, List<GifData> items, @LayoutRes int footerLayout) {
        super(context, items, footerLayout);
    }

    @Override
    protected BaseViewHolder<GifData> createRegularViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.gif_list_item, parent, false);
        return new BaseViewHolder<GifData>(view) {

            ImageView imageView = (ImageView) view.findViewById(R.id.gif);
            TextView trended = (TextView) view.findViewById(R.id.trended);

            @Override
            public void hold(GifData item) {
                Glide.with(getContext())
                        .load(item.getUrl())
                        .asGif()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(imageView);
                Date trendedDate = item.getTrended();
                if (trendedDate != null) {
                    trended.setText(DateFormat.format("HH:mm' 'dd.MM.yyyy", trendedDate));
                } else {
                    trended.setText(R.string.not_trended);
                }
            }
        };
    }
}
